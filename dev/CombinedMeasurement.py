#!/usr/bin/env python
""" Combination of measurements.

Builds a combined measurement from regularised channels.
"""

from __future__ import absolute_import

import logging
import os
import subprocess
import sys

import ROOT

from asimov_utils import *
from utils import *
from AbsMeasurement import AbsMeasurement


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'CombinedMeasurement'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class CombinedMeasurement(AbsMeasurement):
    def __init__(self, name, path=None, wsname=None, mcname=None, dataname=None):
        AbsMeasurement.__init__(self, name, path, wsname, mcname, dataname)

        self._measurements = dict()
        self._pois = ROOT.RooArgSet()
        self._obs = ROOT.RooArgSet()
        self._globs = ROOT.RooArgSet()
        self._nuis = ROOT.RooArgSet()
        self._asimov = None
        self._data = None
        self._parametrisations = None
        self._poi_list = ""

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Created CombinedMeasurement " + self._name)
        existing_logger.info(msg)

    def AddMeasurement(self, measurement):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding Measurement " + measurement.name)
        existing_logger.info(msg)
        self._measurements[measurement.name] = measurement

    @property
    def ParametersOfInterest(self):
        return self._poi_list

    @ParametersOfInterest.setter
    def ParametersOfInterest(self, pois):
        self._poi_list = pois

    @property
    def correlations(self):
        return self._correlations

    @correlations.setter
    def correlations(self, correlations):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Set correlation scheme " + correlations.name)
        existing_logger.info(msg)
        self._correlations = correlations

    @property
    def parametrisations(self):
        return self._parametrisations

    @parametrisations.setter
    def parametrisations(self, parametrisations):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Set ParametrisationSequence " + parametrisations.name)
        existing_logger.info(msg)
        self._parametrisations = parametrisations

    def CollectMeasurements(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Collecting measurements")
        existing_logger.info(msg)
        tmpAllNuisanceParameters = dict()
        for meas in self._measurements:
            measurement = self._measurements[meas]
            measurement.initialise()
            thisNuisanceParameters = measurement.nuis
            tmpAllNuisanceParameters[measurement.name] = thisNuisanceParameters

        # TODO(Correlations) Automatically determine correlations

        for meas in self._measurements:
            measurement = self._measurements[meas]
            measurement.renamings = self._correlations.renamings[meas]
            measurement.parametrisations = self._parametrisations
            measurement.CollectChannels()

            # TODO(Renamings) Propagate back the changes of the renamings

    def CombineMeasurements(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Combining measurements")
        existing_logger.info(msg)

        ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
        ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
        ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

        ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
        ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
        ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooAbsPdf*>::iterator, bool>", "map;string;RooAbsPdf.h")

        if sys.platform != "darwin":
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Generating necessary dictionaries for non-darwin platform")
            existing_logger.debug(msg)

            ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
            ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
            ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

            ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
            ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
            ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooAbsPdf*>::iterator, bool>", "map;string;RooAbsPdf.h")
        # else:
        #     msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Generating necessary dictionaries for Mac")
        #     existing_logger.debug(msg)
        #
        #     fileName = DumpCode('#include <map>\n#include <string>')
        #     command = 'root -l -q -b -e gSystem->CompileMacro(\"%s\",\"k\")*0' % fileName
        #     subprocess.check_output(command.split())
        #     libNameBase = fileName.replace(".C", "_C")
        #     ROOT.gSystem.Load(libNameBase)

        datasetMap = ROOT.std.map('string, RooDataSet*')()
        datasetMap.keepalive = list()
        pdfMap = ROOT.std.map('string, RooAbsPdf*')()
        pdfMap.keepalive = list()

        category = ROOT.RooCategory("master_measurement", "master_measurement")

        numTotPdf = 0

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Defining categories")
        existing_logger.info(msg)

        for meas in self._measurements:
            measurement = self._measurements[meas]
            channels = measurement.channels

            thisObservables = measurement.obs
            thisGlobalObservables = measurement.globs
            thisNuisanceParameters = measurement.nuis

            self._obs.add(thisObservables)
            self._globs.add(thisGlobalObservables)
            self._nuis.add(thisNuisanceParameters)
            self._globs.setAttribAll("GLOBAL_OBSERVABLE")

            for chan in channels:
                channel = channels[chan]
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Defining category " + repr(numTotPdf) + " with name " + channel.name)
                existing_logger.info(msg)
                pdf_tmp = channel.pdf
                data_tmp = channel.data
                category.defineType(channel.name, numTotPdf)
                category.setLabel(channel.name, True)
                pdfMap.keepalive.append(pdf_tmp)
                pdfMap.insert(pdfMap.cbegin(), ROOT.std.pair("const string,RooAbsPdf*")(channel.name, pdf_tmp))
                datasetMap.keepalive.append(data_tmp)
                datasetMap.insert(datasetMap.cbegin(), ROOT.std.pair("const string,RooDataSet*")(channel.name, data_tmp))
                numTotPdf += 1
        self._obs.add(category)

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making combined pdf")
        existing_logger.info(msg)

        self._pdf = ROOT.RooSimultaneous("combPdf", "combPdf", pdfMap, category)
        self._pdf.setStringAttribute("DefaultGlobalObservablesTag", "GLOBAL_OBSERVABLE")

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making combined dataset")
        existing_logger.info(msg)

        weightVar = ROOT.RooRealVar("weightVar2", "", 1.0, -1e10, 1e10)
        obs_cat_weight = ROOT.RooArgSet()
        obs_cat_weight.add(self._obs)
        obs_cat_weight.add(category)
        obs_cat_weight.add(weightVar)

        self._data = ROOT.RooDataSet("combData", "combData", obs_cat_weight, ROOT.RooFit.Index(category), ROOT.RooFit.Import(datasetMap), ROOT.RooFit.WeightVar("weightVar2"))

        self.MakeCleanWorkspace()

    def MakeCleanWorkspace(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making a clean combined workspace")
        existing_logger.info(msg)

        self._pois.sort()
        self._nuis.sort()
        self._globs.sort()
        self._obs.sort()

        nuisItr = self._nuis.createIterator()
        nextNuis = nuisItr.Next()
        while nextNuis:
            if (nextNuis.isConstant()):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Removing nuisances " + nextNuis.GetName() + " from nuisance parameter list (constant)")
                existing_logger.debug(msg)
                self._nuis.remove(nextNuis)

            ClientItr = nextNuis.clientIterator()
            nextClient = ClientItr.Next()
            foundClient = False
            while nextClient:
                foundClient = True
                nextClient = ClientItr.Next()

            if not foundClient:
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Removing nuisances " + nextNuis.GetName() + " from nuisance parameter list (no client)")
                existing_logger.debug(msg)
                self._nuis.remove(nextNuis)

            nextNuis = nuisItr.Next()

        globsItr = self._globs.createIterator()
        nextGlob = globsItr.Next()
        while nextGlob:
            ClientItr = nextGlob.clientIterator()
            nextClient = ClientItr.Next()
            foundClient = False
            while nextClient:
                foundClient = True
                nextClient = ClientItr.Next()

            if not foundClient:
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Removing global observable " + nextGlob.GetName() + " from global observable list (no client)")
                existing_logger.debug(msg)
                self._globs.remove(nextGlob)

            nextGlob = globsItr.Next()

        self._ws = ROOT.RooWorkspace("combined")
        self._ws.autoImportClassCode(True)
        self._mc = ROOT.RooStats.ModelConfig("ModelConfig", self._ws)

        tmpPdfName = self._pdf.GetName()
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
        getattr(self._ws, 'import')(self._pdf, ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.DEBUG)
        self._pdf = self._ws.pdf(tmpPdfName)

        getattr(self._ws, 'import')(self._data)
        if self._asimov is not None:
            getattr(self._ws, 'import')(self._asimov)

        self._mc.SetPdf(self._pdf)
        self.DefineParametersOfInterest(self._mc)
        self._mc.SetParametersOfInterest(self._pois)
        self._mc.SetNuisanceParameters(self._nuis)
        self._mc.SetObservables(self._obs)
        self._mc.SetGlobalObservables(self._globs)

        self._mc.Print()

        getattr(self._ws, 'import')(self._mc)
        self._mc = self._ws.obj("ModelConfig")
        self._obs = self._mc.GetObservables()
        self._globs = self._mc.GetGlobalObservables()
        self._nuis = self._mc.GetNuisanceParameters()
        self._pois = self._mc.GetParametersOfInterest()

    def ParametriseMeasurements(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Re-parametrise the model according " + self._parametrisations.name)
        existing_logger.info(msg)

        # Get the list of schemes for this model from the parametrisation sequence
        sequence = self._parametrisations.sequence

        # Loop through the sequence
        originalName = self._pdf.GetName()

        for scheme in sequence:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "On scheme " + scheme.name)
            existing_logger.info(msg)

            expressions = scheme.expressions
            editStr = "EDIT::" + originalName + "(" + originalName

            for expression in expressions:
                if '=' in expression:
                    editStr += ',' + expression
                else:
                    # msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "New objects: " + re.sub(r'^(.{120}).*$', '\g<1>...', expression))
                    # existing_logger.debug(msg)
                    self._ws.factory(expression)

            editStr += ')'

            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Edit command: " + re.sub(r'^(.{120}).*$', '\g<1>...', editStr))
            existing_logger.debug(msg)
            self._ws.factory(editStr)
            self._pdf = self._ws.pdf(originalName)

            # Add new nuisances and globs
            nuisances = scheme.nuisances
            for p in nuisances:
                if self._ws.var(p):
                    self._nuis.add(self._ws.var(p))

            globs = scheme.globs
            for p in globs:
                if self._ws.var(p):
                    self._globs.add(self._ws.var(p))
            self._globs.setAttribAll("GLOBAL_OBSERVABLE")

        self.MakeCleanWorkspace()

    def DefineParametersOfInterest(self, mc):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Identifying parameters of interest")
        existing_logger.info(msg)

        ws = mc.GetWorkspace()
        oldPois = self._pois
        self._pois.removeAll()

        oldPois.Print()

        pois = self._poi_list.split(",")

        added = set()
        failed = set()

        for poi in pois:
            if ws.var(poi):
                added.add(poi)
                self._pois.add(ws.var(poi))

                if self._nuis.find(ws.var(poi)):
                    self._nuis.remove(self._nuis.find(ws.var(poi)))
            else:
                failed.add(poi)

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Added " + re.sub(r'^(.{120}).*$', '\g<1>...', ", ".join(added)) + " to parameters of interest.")
        existing_logger.info(msg)

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Failed adding " + re.sub(r'^(.{120}).*$', '\g<1>...', ", ".join(failed)) + " to parameters of interest. Not present in combined workspace.")
        existing_logger.warning(msg)

        poiIter = oldPois.createIterator()
        nextPoi = poiIter.Next()
        while nextPoi:
            if (ws.var(nextPoi.GetName()) and not self._pois.find(nextPoi)):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making old poi " + nextPoi.GetName() + " a nuisance parameter")
                existing_logger.debug(msg)
                self._nuis.add(nextPoi)
            nextPoi = poiIter.Next()

    def MakeAsimovData(self, Conditional, profileAt, generateAt):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding Asimov data")
        existing_logger.info(msg)

        muStr = ""
        if profileAt is Snapshot.background:
            muStr = "_0"
        elif profileAt is Snapshot.nominal:
            muStr = "_1"
        elif profileAt is Snapshot.ucmles:
            muStr = "_muhat"
        else:
            print("Unknown value for profiling requested.")
            return

        tmp_ws = ROOT.RooWorkspace("combined")
        tmp_ws.autoImportClassCode(True)
        tmp_mc = ROOT.RooStats.ModelConfig("ModelConfig", tmp_ws)

        tmpPdfName = self._pdf.GetName()
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
        getattr(tmp_ws, 'import')(self._pdf, ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.DEBUG)
        tmp_pdf = tmp_ws.pdf(tmpPdfName)

        getattr(tmp_ws, 'import')(self._data)

        tmp_mc.SetPdf(tmp_pdf)
        tmp_mc.SetParametersOfInterest(self._mc.GetParametersOfInterest())
        tmp_mc.SetNuisanceParameters(self._mc.GetNuisanceParameters())
        tmp_mc.SetObservables(self._mc.GetObservables())
        tmp_mc.SetGlobalObservables(self._mc.GetGlobalObservables())

        getattr(tmp_ws, 'import')(tmp_mc)

        make_asimov_data(tmp_ws, tmp_mc, self._data, Conditional, profileAt, generateAt)

        asimov = tmp_ws.data("asimovData" + muStr)
        getattr(self._ws, 'import')(asimov)

        self._ws.saveSnapshot("nominalGlobs", tmp_ws.getSnapshot("nominalGlobs"), ROOT.kTRUE)
        self._ws.saveSnapshot("nominalNuis", tmp_ws.getSnapshot("nominalNuis"), ROOT.kTRUE)
        self._ws.saveSnapshot("conditionalNuis" + muStr, tmp_ws.getSnapshot("conditionalNuis" + muStr), ROOT.kTRUE)
        self._ws.saveSnapshot("conditionalGlobs" + muStr, tmp_ws.getSnapshot("conditionalGlobs" + muStr), ROOT.kTRUE)


if __name__ == '__main__':
    pass
